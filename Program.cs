﻿using System;
using System.Threading;

namespace Randome_Generator_Beta
{
	class Program
	{
		static void Main(string[] args)
		{
			int start, stop, n;
			string ans = "";
			int[] random;
			bool flag;
			Console.Write("Enter Starting Number: ");
			start = int.Parse(Console.ReadLine());
			Console.Write("Enter Ending Number: ");
			stop = int.Parse(Console.ReadLine());
			Console.Write("How Many Random Numbers Needed? ");
			n = int.Parse(Console.ReadLine());
			if (n - 1 <= (stop - start))
			{
				Console.Write("Allow Duplication in Randome Numbers(Y/N)? ");
				ans = Console.ReadLine();
			}
			else
			{
				ans = "y";
			}
			random = new int[n];
			if (ans == "n" || ans == "N")
			{
				for (int i = 0; i < n;)
				{
					flag = false;
					Random rand = new Random();
					int rnd = rand.Next(start, stop + 1);
					for (int j = 0; j < i; j++)
					{
						if (random[j] == rnd)
						{
							flag = true;
						}
					}
					if (flag == false)
					{
						random[i] = rnd;
						i++;
					}
				}
			}
			else
			{
				for (int i = 0; i < n; i++)
				{
					Random rand = new Random();
					random[i] = rand.Next(start, stop + 1);
					Thread.Sleep(58);
				}
			}

			for (int i = 0; i < n; i++)
			{
				Console.Write("{0}\t", random[i]);
			}
			Console.ReadKey();
		}
	}
}
